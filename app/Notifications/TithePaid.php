<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Channels\SmsChannel;
use Illuminate\Support\Facades\Log;

class TithePaid extends Notification implements ShouldQueue
{
    use Queueable;
    public $senderName;
    public $phoneNumber;
    public $smsMessage;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($senderName,$phoneNumber, $smsMessage)
    {
        $this->senderName = $senderName;
        $this->phoneNumber = $phoneNumber;
        $this->smsMessage = $smsMessage;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SmsChannel::class];
    }

    public function toSms($notifiable)
    {
        return [
            'smsMessage' => $this->smsMessage,
            'phoneNumber' => $this->phoneNumber,
            'senderName' => $this->senderName
        ];
    }

}
