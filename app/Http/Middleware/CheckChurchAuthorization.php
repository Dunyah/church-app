<?php

namespace App\Http\Middleware;

use Closure;
use App\Church;
use SebastianBergmann\Environment\Console;

class CheckChurchAuthorization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Checks if the user is authenticated and statue is not pending
        $user = $request->user();

            if(!$user || $user->Church->status == "PENDING"){
                abort(403);
            }
            return $next($request);




    }
}
