<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Announcement;
use Illuminate\Support\Facades\Validator;

class AnnouncementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Announcement::latest()->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'title'=> 'required',
            'description' => 'required'
        ]);
       
          // get image extension from base64 image
            if($request->photo){
                $name = time().'.'.explode('/', explode(':', substr($request->photo, 0 , strpos($request->photo, ';')))[1])[1];
            }
        // save image into the public folder
        \Image::make($request->photo)->save(public_path('/img/announce/').$name);
    // Replace the photo with the new name generated
        $request->merge(['photo' => $name]);


        $result = Announcement::create([
            'title'=> $request['title'],
            'description' => $request['description'],
            'photo' => $request['photo']
        ]);
        if($result){
          return response()->json(['status' => true, 'message' => 'Announcement added'], 200);     
        }else{
            return response()->json(['status' => false, 'message' => 'Announcement not added'], 200);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $announce = Announcement::findOrfail($id);

        // Check if image doesnt exist
      $currentPhoto = $announce->photo;
      if($request->photo != $currentPhoto){
            $name = time().'.'.explode('/', explode(':', substr($request->photo, 0 , strpos($request->photo, ';')))[1])[1];
            \Image::make($request->photo)->save(public_path('/img/announce/').$name);
            $request->merge(['photo' => $name]);
                
            // delete announce previous photo 
            $announceOldPhoto = public_path('/img/announce/').$currentPhoto;
            if(file_exists($announceOldPhoto)){
                // deletes announce old photo from pulic directory
               @unlink($announceOldPhoto);
            }
      }
     
       $validator = Validator::make($request->all(),[
                        'title'=> 'required',
                         'description'=> 'required'
                    ]);
         if($validator->fails()){
            return response()->json(['status' => false, 'message' => $validator->errors()->first()],422);           
            }
           
            $res = $announce->update($request->all());
            if($res){
                return response()->json(['status' => true, 'message' =>'Announcement updated successfully'], 200);      
            }else{
                return response()->json(['status' => false, 'message' =>'Announcement not updated'], 200);   
            } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $announce = Announcement::findOrfail($id);
 
        // delete announce photo 
        $currentPhoto = $announce->photo;
        $announcePhoto = public_path('/img/announce/').$currentPhoto;
               if(file_exists($announcePhoto)){
                   // deletes announce old photo from pulic directory
                  @unlink($announcePhoto);
               }
       //if exist delete
       $result = $announce->delete();
       if($result){
        return response()->json(['status' => true, 'message' => 'Announcement deleted successfully'], 200);     
       }
       else{
        return response()->json(['status' => false, 'message' => 'Deletion failed'], 200);
       }
    }
}
