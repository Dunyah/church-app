<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ReportController extends Controller
{

    public function summary(Request $request){

        $this->validate($request, [
            'year' => 'required',
            'startingMonth' => 'required',
            'endingMonth' => 'required'
        ]);


        $year = $request->get('year');
        $startingMonth = $request->get('startingMonth');
        $endingMonth = $request->get('endingMonth');
//Carbon::now()->subMonth()->startOfMonth()
        $startDate = Carbon::createFromDate($year, $startingMonth, 0);
     //   $end = (new Carbon('last day of last month'));
        $endDate = Carbon::createFromDate($year, $endingMonth, 32);
        $data = $request->user()->church->attendances()->whereYear('datePresent','=',$year)->whereBetween('datePresent', [$startDate,$endDate])->get()->groupBy(function($val) {
            return Carbon::parse($val->datePresent)->format('M');
            // $monthObj->groupBy(function($week){
            //    return Carbon::parse($week->datePresent)->format('W');
            // });
            // return $monthObj->format('M');
      });

      $data = collect($data)->map(function($month){
            return collect($month)->groupBy(function($val){
                return Carbon::parse($val->datePresent)->weekOfMonth;
            })->map(function($week){
                return $week->sum('numberOfPeople');
            });
      });




       return response()->json(['status' => true, 'data' => $data]);

        // select from the members attendance where year is the given year,
        // start month is the given starting month
        // end month is the given ending month

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
