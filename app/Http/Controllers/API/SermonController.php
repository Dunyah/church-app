<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Sermon;
use Carbon\Carbon;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use SebastianBergmann\Environment\Console;

class SermonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'datepreached' => 'required'
        ]);

        $audioPath = null;
        if($validator->fails()){
            return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
            }


            if($request->file('audio')){
               $audio = $request->file('audio');
             // $fileName = time().'.'. $audio->getClientOrOriginalExtension();

           //  $audio->move($path, $filename);

               $path = Storage::disk('public')->putFile('audios', $request->file('audio'));
               $audioPath = $path;
            }

        $result = Sermon::create([
           'title' => $request['title'],
           'description' => $request['description'],
           'audio' => $audioPath,
           'datepreached'=>Carbon::parse($request['datepreached'])->toDateTimeString(),
           'church_id'=>$request['church_id']
        ]);

        if($result){
            return response()->json(['status' => true, 'message' => 'Sermon added successfully'],200);
        }
        else{
            return response()->json(['status' => false, 'message' => 'Sermon not added successfully'],200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Sermon::where('church_id','=',$id)->latest()->get();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sermon  = Sermon::findorfail($id);
        $validator = Validator::make($request->all(), [
            'id'=>'required',
            'title' => 'required',
            'description' => 'required',
            'datepreached' => 'required'


        ]);

        if($validator->fails()){
            return response()->json(['status' => false, 'message' => $validator->errors()->first()],422);
            }
          $request['datepreached'] = Carbon::parse($request['datepreached'])->toDateTimeString();
        //  return $request->datepreached;

            $res = $sermon->update($request->all());
            if($res){
                return response()->json(['status' => true, 'message' =>'Sermon updated successfully'], 200);
            }else{
                return response()->json(['status' => false, 'message' =>'Sermon not updated'], 200);
            }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sermon = Sermon::findorfail($id);
        $result = $sermon->delete();
        if($result){
            return response()->json(['status' => true, 'message' => 'Sermon deleted successfully'], 200);
           }
           else{
            return response()->json(['status' => false, 'message' => 'Deletion failed'], 200);
           }
    }
}
