<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Church;

class ChurchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Church::latest()->get();
    }

    public function RegisterChurch(Request $request){
        $validator = Validator::make($request->all(), [
            'churchName' => 'required',
            'contactNumber' => 'required|unique:churches'
        ]);

        if($validator->fails()){
            return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
            }

         $result = Church::create([
            'churchName' => $request['churchName'],
            'contactNumber' => $request['contactNumber'],
            'status' =>'PENDING',
            'url'=>$request['url']
         ]);
         if($result){
            return response()->json(['status' => true, 'message' => 'Church created. You will be notified shortly'],200);
        }
        else{
            return response()->json(['status' => false, 'message' => 'Church not created'],200);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'churchName' => 'required',
            'contactNumber' => 'required|unique:churches'
        ]);

        if($validator->fails()){
            return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
            }

         $result = Church::create([
            'churchName' => $request['churchName'],
            'contactNumber' => $request['contactNumber'],
            'status' =>'PENDING'
         ]);
         if($result){
            return response()->json(['status' => true, 'message' => 'Church created. You will be notified shortly'],200);
        }
        else{
            return response()->json(['status' => false, 'message' => 'Church not created'],200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $church = Church::findorfail($id);

        $validator = Validator::make($request->all(), [
            'churchName' => 'required',
            'contactNumber' => 'required' //|unique:churches,contactNumber,'.$church->$id
           // 'phoneNumber' => 'required|unique:members,phoneNumber,'.$member->id
        ]);

        if($validator->fails()){
            return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
            }

           $result = $church->update($request->all());
           if($result){
            return response()->json(['status' => true, 'message' =>'Church updated'], 200);
            }else{
                return response()->json(['status' => false, 'message' =>'Church not updated'], 200);
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $church = Church::findorfail($id);

         $result = $church->delete();
         if($result){
            return response()->json(['status' => true, 'message' => 'Church deleted successfully'], 200);
           }
           else{
            return response()->json(['status' => false, 'message' => 'Deletion failed'], 200);
           }
    }

    public function GetChurchByName(Request $request){
          $church = Church::where([
              ['churchName','like', '%'.$request->churchName.'%'],
              ['status','=', 'AUTHORIZED']
          ])->get();
          return $church;
    }
}
