<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Testimony;
use App\Member;
use App\User;
use Illuminate\Support\Facades\Validator;

class TestimonyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $churchID = $request->user()->church_id;
        $testimony = Testimony::with('member')->where([
            ['status','=','PENDING'],
            ['church_id', '=', $churchID]
            ])->latest()->get();
         return  $testimony;
    }

    public function getAuthorizedTestimonies(Request $request){

        $churchID = $request->user()->church_id;
        $testimony = Testimony::with('member')->where([
            ['status','=','AUTHORIZED'],
            ['church_id','=',$churchID]
            ])->latest()->get();
        //  $allTestimony = $testimony->with('user')->get();
         return  $testimony;
    }
    public function countPendingTestimonies(Request $request){
        $churchID = $request->user()->church_id;
        $Totaltestimony = Testimony::with('member')->where([
            ['status','=','PENDING'],
            ['church_id','=',$churchID]
            ])->latest()->get();
        return $Totaltestimony->groupBy('id')->count();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(['status' => false, 'message' => $validator->errors()->first()],422);
            }
            $churchID = $request->user()->church_id;

           $result = Testimony::create([
            'description' => $request['description'],
            'member_id' => $request['memberId'],
            'user_id' => $request['userId'],
            'status' => 'PENDING',
            'church_id' => $churchID
           ]);

           if($result){
            return response()->json(['status' => true, 'message' => 'Testimony added successfully'],200);
            }
            else{
                return response()->json(['status' => false, 'message' => 'Testimony not added successfully'],200);
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $testimony  = Testimony::findorfail($id);
        $validator = Validator::make($request->all(), [
            'id'=>'required',
            'description' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(['status' => false, 'message' => $validator->errors()->first()],422);
            }


            $res = $testimony->update($request->all());
            if($res){
                return response()->json(['status' => true, 'message' =>'Testimony updated successfully'], 200);
            }else{
                return response()->json(['status' => false, 'message' =>'testimony not updated'], 200);
            }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $testimony = Testimony::findOrfail($id);
        $result = $testimony->delete();
        if($result){
         return response()->json(['status' => true, 'message' => 'Testimony deleted successfully'], 200);
        }
        else{
         return response()->json(['status' => false, 'message' => 'Deletion failed'], 200);
        }
    }
}
