<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Member;
use App\User;
use Illuminate\Support\Facades\Validator;
use Excel;
use Illuminate\Support\Facades\DB;
use App\Imports\MemberImport;
use Exception;
use Illuminate\Support\Facades\Log;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $churchID =  $request->user()->church_id;
       return Member::with('user')->where('church_id','=',$churchID)->latest()->get();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
                $validator = Validator::make($request->all(),[
                    'name'=> 'required',
                    'phoneNumber' => 'required|unique:members'
                ]);
                if($validator->fails()){
                return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
                }

          // get image extension from base64 image
            if($request->photo){
                $name = time().'.'.explode('/', explode(':', substr($request->photo, 0 , strpos($request->photo, ';')))[1])[1];

                // save image into the public folder
            \Image::make($request->photo)->save(public_path('/img/profile/').$name);
            // Replace the photo with the new name generated
                $request->merge(['photo' => $name]);
            }



        $result = Member::create([
            'name'=> $request['name'],
            'phoneNumber' => $request['phoneNumber'],
            'occupation' => $request['occupation'],
            'location' => $request['location'],
            'contactPerson' => $request['contactPerson'],
            'placeOfWork' => $request['placeOfWork'],
            'MaritalStatus' => $request['MaritalStatus'],
            'contactPersonPhoneNumber' => $request['contactPersonPhoneNumber'] ,
            'Gender' => $request['Gender'] ,
            'photo' => $request['photo'],
            'user_id' => $request['userId'],
            'church_id' => $request->user()->church_id
        ]);
        if($result){
          return response()->json(['status' => true, 'message' => 'Member created'], 200);
        }else{
            return response()->json(['status' => false, 'message' => 'Member not created'], 200);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      //  return Member::with('user')->where('church_id','=',$id)->latest()->get();
    }

    public function GetMembersByChurchId(Request $request){
        return Member::with('user')->where('church_id','=',$request['church_id'])->latest()->get();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $member = Member::findOrfail($id);

        // Check if image doesnt exist
      $currentPhoto = $member->photo;
      if($request->photo != $currentPhoto){
            $name = time().'.'.explode('/', explode(':', substr($request->photo, 0 , strpos($request->photo, ';')))[1])[1];
            \Image::make($request->photo)->save(public_path('/img/profile/').$name);
            $request->merge(['photo' => $name]);

            // delete member previous photo
            $memberOldPhoto = public_path('/img/profile/').$currentPhoto;
            if(file_exists($memberOldPhoto)){
                // deletes member old photo from pulic directory
               @unlink($memberOldPhoto);
            }
      }

      $validator = Validator::make($request->all(),[
                        'name'=> 'required',
                        'phoneNumber' => 'required|unique:members,phoneNumber,'.$member->id
                    ]);
         if($validator->fails()){
            return response()->json(['status' => false, 'message' => $validator->errors()->first()],422);
            }

            $res = $member->update($request->all());
            if($res){
                return response()->json(['status' => true, 'message' =>'Member updated successfully'], 200);
            }else{
                return response()->json(['status' => false, 'message' =>'Member not updated'], 200);
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $member = Member::findOrfail($id);

        // delete member photo
        $currentPhoto = $member->photo;
        $memberPhoto = public_path('/img/profile/').$currentPhoto;
               if(file_exists($memberPhoto)){
                   // deletes member old photo from pulic directory
                  @unlink($memberPhoto);
               }
       //if exist delete
       $result = $member->delete();
       if($result){
        return response()->json(['status' => true, 'message' => 'Member deleted successfully'], 200);
       }
       else{
        return response()->json(['status' => false, 'message' => 'Deletion failed'], 200);
       }

    }

    public function totalmembers(Request $request){
        $totalMembers = Member::where('church_id', '=', $request->user()->church_id)->latest()->get();
         $result = $totalMembers->groupBy('id')->count();
         return $result;

    }

    public function getMemberbyName(Request $request){

        $churchID =  $request->user()->church_id;
        $member = Member::where([
            ['name', 'like','%'.$request->name.'%'],
            ['church_id', '=', $churchID]
        ])->get();
         return $member;
    }

    public function ImportExcelData(Request $request){
        $validator = Validator::make($request->all(),[
            'member_upload'=> 'required|mimes:xls,xlsx'
        ]);
            if($validator->fails()){
            return response()->json(['status' => false, 'message' => $validator->errors()->first()],422);
            }


           try{

            $result = Excel::import(
                new MemberImport($request->user()->id,$request->user()->church_id),
                $request->file('member_upload')
            );

            if($result){
                return response()->json(['status' => true, 'message' => 'Members uploaded successfullly'], 200);
                }else{
                    return response()->json(['status' => false, 'message' => 'Members not created'], 200);
            }

           }catch(\Exception $e){
            Log::info('Import exception: ' . $e->getMessage());
           }
    }

}
