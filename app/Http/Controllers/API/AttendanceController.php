<?php

namespace App\Http\Controllers\API;

use App\Attendance;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $churchID =  $request->user()->church_id;
        return Attendance::with('user')->where('church_id','=',$churchID)->orderBy('datePresent', 'desc')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'numberOfPeople' => 'required',
            'datePresent' => 'required'
        ]); ///'',''
        if($validator->fails()){
            return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
            }
          $result = Attendance::create([
                   'numberOfPeople' => $request["numberOfPeople"],
                   'datePresent'=>Carbon::parse($request['datePresent'])->toDateTimeString(),
                   'church_id'=>$request['church_id'],
                   'user_id' => $request['user_id']
                ]);

        if($result){
            return response()->json(['status' => true, 'message' => 'Attendance submitted!'],200);
        }
        else{
            return response()->json(['status' => false, 'message' => 'Attendance not submitted!'],200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request  $request, $id)
    {
         $churchID =  $request->user()->church_id;
         return Attendance::with('user')->where('church_id','=',$churchID)->latest()->get();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $attendance = Attendance::findorfail($id);

        $validator = Validator::make($request->all(), [
            'numberOfPeople' => 'required',
            'datePresent' => 'required'
        ]); ///'',''
        if($validator->fails()){
            return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
            }
            $request['datePresent'] = Carbon::parse($request['datePresent'])->toDateTimeString();

           $result =  $attendance->update($request->all());
           if($result){
            return response()->json(['status' => true, 'message' => 'Attendance updated!'],200);
        }
        else{
            return response()->json(['status' => false, 'message' => 'Attendance not updated!'],200);
        }
    }

    public function GetLatestAttendance(Request $request){
         return Attendance::where('church_id','=', $request->user()->church_id)->orderBy('datePresent', 'desc')->first();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
