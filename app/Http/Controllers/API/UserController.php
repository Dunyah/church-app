<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Church;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return User::with('Church')->where('church_id','!=', 0)->latest()->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $register =  $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|string',
            'church_id' => 'required'
       ]);


        // Register User
        $user = User::create([
            "name"=> $request["name"],
            "email" => $request["email"],
            "password" => Hash::make($request["password"]) ,
            "church_id" => $request["church_id"],
            "password_status" =>"NEW"
        ]);

        if($user){
        return response()->json(['status' => true, 'message' => 'Admin created!'], 200);
        }
        else{
           return response()->json(['status' => false, 'message' => 'server down. Please try again'], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $user = User::findOrfail($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'church_id' => 'required',
            'id' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
            }

            $result = $user->update($request->all());
            if($result){
                return response()->json(['status' => true, 'message' => 'Admin updated!'], 200);
             }
             else{
                return response()->json(['status' => false, 'message' => 'Admin not updated!'], 200);
             }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrfail($id);
        $result = $user->delete();
        if($result){
            return response()->json(['status' => true, 'message' => 'Admin deleted!'], 200);
         }
         else{
            return response()->json(['status' => false, 'message' => 'Admin not deleted!'], 200);
         }

    }

    public function login(Request $request){
        $login = $this->validate($request, [
             "email"=> "required|email",
             "password" => "required"
         ]);

       if(!Auth::attempt($login)){
         return response(['message' =>'Invalid email or password', 'status' => false]);
       }
       // iF Super Admin do not check for church status
        if($request['role'] != 'SUPERADMIN') {
            if(Auth::user()->Church->status == "PENDING"){
                Auth::logout();
             return response(['message' =>'Your church is not yet authorized.Contact admin', 'status' => false]);
            }
        }
       // Authenticate user  and get token and user details
       $accessToken = Auth::user()->createToken('jwt')->accessToken;
       return response(['user'=> Auth::user(), 'jwt' => $accessToken, 'status'=> true]);

    }


    public function ResetPassword(Request $request, $id){
         $user = User::findOrfail($id);
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);
        if($validator->fails()){
            return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
            }
             $request["password_status"] = "RESET";
              $request["password"] = Hash::make($request["password"]);

            $result = $user->update($request->all());
            if($result){
                return response()->json(['status' => true, 'message' => 'Password Reset!'], 200);
             }
             else{
                return response()->json(['status' => false, 'message' => 'Password Not Reset!'], 200);
             }
    }
}
