<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Member;
use App\Tithe;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use App\Notifications\TithePaid;

class TitheController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $churchID = $request->user()->church_id;
      return Tithe::with('member')->where('church_id','=',$churchID)->latest()->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'member_id' => 'required',
            'amount' => 'required',
            'datepaid' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(['status' => false, 'message' => $validator->errors()->first()],422);
            }

          //  return response()->json(['status' => true, 'message' => $request['phoneNumber']],200);

            $result = Tithe::Create([
             'member_id' => $request['member_id'],
             'amount' => $request['amount'],
             'datepaid' => Carbon::parse($request['datepaid'])->toDateTimeString(),
             'user_id' => $request['userId'],
             'church_id' => $request->user()->church_id
           ]);


            if($result){
                     // Send SMS  || since it is notification is will run the app b4 handlying it
                getSystemUser()->notify(new TithePaid('LightTemple',$request['phoneNumber'], $request['smsMessage']));
                return response()->json(['status' => true, 'message' => 'Tithe added successfully'],200);
            }
            else{
                return response()->json(['status' => false, 'message' => 'Tithe not added successfully'],200);
            }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tithe = Tithe::findorfail($id);
        $validator = Validator::make($request->all(), [
            'member_id' => 'required',
            'amount' => 'required',
            'datepaid' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(['status' => false, 'message' => $validator->errors()->first()],422);
            }
           $request['datepaid'] = Carbon::parse($request['datepaid'])->toDateTimeString();

            $res = $tithe->update($request->all());
            if($res){
                return response()->json(['status' => true, 'message' =>'Tithe updated successfully'], 200);
            }else{
                return response()->json(['status' => false, 'message' =>'Tithe not updated'], 200);
            }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getTotalSumOfWeeksTithes(Request $request){
        $churchID = $request->user()->church_id;
       $totalTithes = Tithe::where('church_id','=',$churchID)->whereBetween('created_at', [Carbon::now()->subMonth()->startOfMonth(), Carbon::now()->subMonth()->endOfMonth()])->sum('amount');
      // Carbon::setWeekStartsAt(Carbon::SUNDAY);
       //Carbon::setWeekEndsAt(Carbon::SATURDAY);
       return [
           'sum' =>$totalTithes,
           'month' => Carbon::now()->subMonth()->format('F')
       ];
    }
}
