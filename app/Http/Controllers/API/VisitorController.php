<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Visitor;
use Illuminate\Support\Facades\Validator;

class VisitorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return Visitor::where('church_id','=',$request->user()->church_id)->latest()->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name'=> 'required',
            'phoneNumber' => 'required|unique:visitors',
            'invitedBy' => 'required'
        ]);
        if($validator->fails()){
        return response()->json(['status' => false, 'message' => $validator->errors()->first()],422);
        }

        $res = Visitor::create([
            "name" => $request["name"],
            "phoneNumber" => $request["phoneNumber"],
            "houseNo" => $request["houseNo"],
            "emailAddress" => $request["emailAddress"],
            "postalAddress" => $request["postalAddress"],
            "invitedBy" => $request["invitedBy"],
            "specificProblem" => $request["specificProblem"],
            "prayerRequest" => $request["prayerRequest"],
            "worshipwithusagain" => $request["worshipwithusagain"],
            "ifNoWhy" => $request["ifNoWhy"],
            "remarks" => $request["remarks"],
            "church_id" => $request->user()->church_id
         ]);
         if($res){
           return response()->json(['status' => true, 'message' => 'Visitor created'], 200);
         }else{
           return response()->json(['status' => false, 'message' => 'Visitor not created'], 200);
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $visitor = Visitor::findOrfail($id);

        $validator = Validator::make($request->all(),[
            'name'=> 'required',
            'phoneNumber' => 'required|unique:visitors,phoneNumber,'.$visitor->id,
            'invitedBy' => 'required'
        ]);
        if($validator->fails()){
        return response()->json(['status' => false, 'message' => $validator->errors()->first()],422);
        }

        $result = $visitor->update($request->all());
         if($result){
            return response()->json(['status' => true, 'message' => 'Visitor updated']);
         }
        return response()->json(['status' => false, 'message' => 'Unable to update Visitor']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $visitor = Visitor::findOrfail($id);
        //if exist delete
        $result = $visitor->delete();
        if($result){
          return response()->json(['status' => true, 'message'=>'Visitor deleted'],200);
        }else{
            return response()->json(['status' => false, 'message'=>'Visitor not deleted'],200);
        }
    }
}
