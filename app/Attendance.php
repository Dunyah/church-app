<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $fillable = [
        'id','datePresent','numberOfPeople','church_id','user_id'
    ];

    public function church(){
        return $this->belongsTo('App\Church');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
