<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Church extends Model
{
   protected $fillable =[
       'id','churchName','contactNumber','status','url'
   ];

   public function users(){
       return $this->hasMany('App\User');
   }

   public function members(){
       return $this->hasMany('App\Member');
   }

   public function attendances(){
       return $this->hasMany('App\Attendance');
   }

}
