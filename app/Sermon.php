<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sermon extends Model
{
    protected $fillable = [
        'id','title','description','audio','datepreached','church_id'
       ];
}
