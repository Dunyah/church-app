<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimony extends Model
{
    protected $fillable = [
        'id','description','views','status','member_id','user_id','church_id'
    ];

    public function member(){
        return $this->belongsTo('App\Member');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
