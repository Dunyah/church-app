<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
    protected $fillable = [
        'id',
        'name',
        'phoneNumber',
        'houseNo',
        'emailAddress',
        'postalAddress',
        'invitedBy',
        'specificProblem',
        'prayerRequest',
        'worshipwithusagain',
        'ifNoWhy',
        'remarks',
        'church_id'
    ];
}
