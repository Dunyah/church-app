<?php

namespace App\Imports;

use App\Member;
use GuzzleHttp\Psr7\Request;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class MemberImport implements ToCollection
{
    private $userId;
    private $churchId;

    function __construct($userId,$churchId) {
        $this->userId = $userId;
        $this->churchId = $churchId;
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    // public function model(array $row)
    // {
    //     return new Member([
    //         'name'     => $row[0],
    //         'phoneNumber' => $row[1],
    //         'user_id' => $this->userId,
    //         'church_id' => $this->churchId
    //     ]);
    // }


    /**
     * @param Collection $rows
     */
    public function collection(Collection $rows)
    {
        $dataToInsert = [];

        foreach ($rows as $row) {
            array_push($dataToInsert, [
                'name'     => $row[0],
                'phoneNumber' => $row[1],
                'user_id' => $this->userId,
                'church_id' => $this->churchId
            ]);
        }

       (new Member())->insertOrUpdate($dataToInsert);

    }
}
