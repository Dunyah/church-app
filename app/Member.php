<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Member extends Model
{
    protected $fillable = [
        'id','name','phoneNumber','occupation','placeOfWork','location','contactPerson','MaritalStatus','contactPersonPhoneNumber','Gender','photo','user_id','church_id'
       ];

       public function tithes(){
        return $this->hasMany('App\Tithe');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function testimonies(){
        return $this->hasMany('App\Testimony');
    }

    public function church(){
        return $this->belongsTo('App\Church');
    }

    /**
     * Mass (bulk) insert or update on duplicate for Laravel 4/5
     *
     * insertOrUpdate([
     *   ['id'=>1,'value'=>10],
     *   ['id'=>2,'value'=>60]
     * ]);
     *
     *
     * @param array $rows
     * @return
     */
    function insertOrUpdate(array $rows){
        $table = DB::getTablePrefix().with(new self)->getTable();


        $first = reset($rows);

        $columns = implode( ',',
            array_map( function( $value ) { return "$value"; } , array_keys($first) )
        );

        $values = implode( ',', array_map( function( $row ) {
                return '('.implode( ',',
                        array_map( function( $value ) { return '"'.str_replace('"', '""', $value).'"'; } , $row )
                    ).')';
            } , $rows )
        );

        $updates = implode( ',',
            array_map( function( $value ) { return "$value = VALUES($value)"; } , array_keys($first) )
        );

        $sql = "INSERT INTO {$table}({$columns}) VALUES {$values} ON DUPLICATE KEY UPDATE {$updates}";

        return DB::statement($sql);
    }
}
