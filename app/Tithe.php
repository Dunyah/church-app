<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tithe extends Model
{
    protected $fillable = [
    'id','member_id','user_id','datepaid','amount','church_id'
    ];

    public function member(){
        return $this->belongsTo("App\Member");
       }

       public function user(){
        return $this->belongsTo("App\User");
       }
}
