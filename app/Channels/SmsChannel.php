<?php

namespace App\Channels;

use Illuminate\Notifications\Notification;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class SmsChannel
{
    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        $array = $notification->toSms($notifiable);

        Log::info('sending sms: ' . json_encode($array));

        $client = new Client();
        try{

            $client->request('POST', 'https://eazisend.com/api/sms/single', [
                'form_params' => [
                            'clientId' => '2eae0c0c-4938-426d-a459-a01fa737e59d',
                            'apiKey' => '$2y$10$FYUYz1hgUby1NK4kyeAjsu9wEbYCusVHL/cY5Uvj.RVJbS0dl7BPq',
                            'senderName' => $array['senderName'],
                            'phoneNumber' => $array['phoneNumber'],
                            'message' => $array['smsMessage']
                ],
                'headers' => [
                    'Accept'     => 'application/json'
                ]
            ]);

        }catch(\Exception $e){

        }
    }
}
