<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable,HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','name', 'email', 'password','church_id','password_status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function members(){
        return $this->hasMany("App\Member");
       }

       public function tithes(){
        return $this->hasMany('App\Tithe');
    }

    public function testimonies(){
        return $this->hasMany('App\Testimony');
    }

    public function church(){
        return $this->belongsTo('App\Church');
    }
    public function attendances(){
        return $this->hasMany('App\Attendance');
    }
}
