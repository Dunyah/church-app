<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('test', function(){

//   $file = Storage::url('/audios/RbXz1WewcbGipZuPoTQN8Gp6j02ihjvcbSo0V2f4.mp4'); //\Illuminate\Support\Facades\Storage::url();
//   return '<audio controls>
//     <source src="'.$file.'" type="audio/mpeg">
//     Your browser does not support the audio element.
//     </audio>';
// });
// Route::get('/', function(){
// return "hello laravel";

Route::get('/{any}', 'ApplicationController')->where('any', '.*');
