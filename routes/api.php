<?php

use App\Http\Middleware\CheckChurchAuthorization;


// use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


 Route::post('/RegisterChurch', 'API\ChurchController@RegisterChurch');
 Route::post('/login', 'API\UserController@login');
 Route::post('/admin/ch/login','API\UserController@login');

Route::middleware("auth:api",CheckChurchAuthorization::class)->group(function(){
    Route::apiResources(['member'=> 'API\MemberController']);
    Route::apiResources(['visitor'=> 'API\VisitorController']);
    Route::apiResources(['sermon'=> 'API\SermonController']);
    Route::get('totalmembers', 'API\MemberController@totalmembers');

    Route::apiResources(['tithe' => 'API\TitheController']);
    Route::get('getMembers', 'API\TitheController@getMembers');
    Route::post('getMemberbyName', 'API\MemberController@getMemberbyName');
    Route::post('excelUpload', 'API\MemberController@ImportExcelData');
    Route::post('summary', 'API\ReportController@summary');


    Route::apiResources(['announcement' => 'API\AnnouncementController']);
    Route::apiResources(['testimony' => 'API\TestimonyController']);
    Route::apiResources(['attendance' => 'API\AttendanceController']);
    Route::get('GetLatestAttendance', 'API\AttendanceController@GetLatestAttendance');

    Route::get('getAuthorizedTestimonies', 'API\TestimonyController@getAuthorizedTestimonies');
    Route::get('countPendingTestimonies', 'API\TestimonyController@countPendingTestimonies');
    Route::get('getTotalSumOfWeeksTithes', 'API\TitheController@getTotalSumOfWeeksTithes');
    //
    Route::post('ResetPassword/{id}', 'API\UserController@ResetPassword');


});

Route::group(['middleware' => ['auth:api']], function () {
    Route::apiResources(['church' => 'API\ChurchController']);
    Route::post('GetChurchByName', 'API\ChurchController@GetChurchByName');
    Route::apiResources(['register' => 'API\UserController']);

});
