/*=========================================================================================
  File Name: router.js
  Description: Routes for vue-router. Lazy loading is enabled.
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)
window.Fire = new Vue()

import VeeValidate from 'vee-validate';
Vue.use(VeeValidate);

import VueToastr2 from 'vue-toastr-2'
import 'vue-toastr-2/dist/vue-toastr-2.min.css'
window.toastr = require('toastr')
Vue.use(VueToastr2)


import Swal from 'sweetalert2'
window.Swal = Swal

import LoadScript from 'vue-plugin-load-script';
Vue.use(LoadScript);


// Vue.loadScript("/assets/js/vendor/jquery-1.12.4.min.js")

import vueDebounce from 'vue-debounce'
Vue.use(vueDebounce, {
  listenTo: ['input', 'keyup'],
  lock: true,
  defaultTime: '300ms',
  fireOnEmpty: false
})


const authUser = {
    headers: {Authorization:'Bearer ' + localStorage.getItem('jwt')}
    }
  window.auth = authUser;

  let isAuth = false;
isAuth = localStorage.getItem("jwt") !== null ? true : false;



import VueFileAgent from 'vue-file-agent';
import VueFileAgentStyles from 'vue-file-agent/dist/vue-file-agent.css';
Vue.use(VueFileAgent);

const axios = require('axios').default;
window.axios =axios

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    scrollBehavior () {
        return { x: 0, y: 0 }
    },
    routes: [

        {
    // =============================================================================
    // MAIN LAYOUT ROUTES
    // =============================================================================
            path: '/',
            component: () => import('./layouts/main/Main.vue'),
            beforeEnter: (to, from, next) =>{
              console.log("before Enter ", isAuth);
              if(isAuth && auth !== undefined && localStorage.getItem("password_status") !== "NEW"){
                next()
              }
             else{ next("/login") }
           },
            children: [
        // =============================================================================
        // Theme Routes
        // =============================================================================
              {
                path: '/',
                name: 'home',
                component: () => import('./views/Home.vue')
              },
              {
                path: '/addMember',
                name: 'addMemeber',
                component: () => import('./views/pages/members/AddMember.vue')
              },
              {
                path: '/editMember',
                name: 'EditMember',
                component: () => import('./views/pages/members/EditMember.vue')
              },
              {
                path: '/viewMember',
                name: 'ViewMember',
                component: () => import('./views/pages/members/ViewMember.vue')
              },
              {
                path: '/uploadMember',
                name: 'UploadMember',
                component: () => import('./views/pages/members/UploadMembers.vue')
              },
              {
                path: '/addVisitor',
                name: 'AddVisitor',
                component: () => import('./views/pages/visitors/AddVisitor.vue')
              },
              {
                path: '/editVisitor',
                name: 'EditVisitor',
                component: () => import('./views/pages/visitors/EditVisitor.vue')
              },
              {
                path: '/viewVisitor',
                name: 'ViewVisitor',
                component: () => import('./views/pages/visitors/ViewVisitor.vue')
              },
              {
                path: '/addSermon',
                name: 'AddSermon',
                component: () => import('./views/pages/sermons/AddSermon.vue')
              },
              {
                path: '/editSermon',
                name: 'EditSermon',
                component: () => import('./views/pages/sermons/EditSermon.vue')
              },
              {
                path: '/viewSermon',
                name: 'ViewSermon',
                component: () => import('./views/pages/sermons/ViewSermon.vue')
              },
              {
                path: '/addAnnouncement',
                name: 'AddAnnouncement',
                component: () => import('./views/pages/announcements/AddAnnounce.vue')
              },
              {
                path: '/viewAnnouncement',
                name: 'ViewAnnouncement',
                component: () => import('./views/pages/announcements/ViewAnnounce.vue')
              },
              {
                path: '/editAnnouncement',
                name: 'EditAnnouncement',
                component: () => import('./views/pages/announcements/EditAnnounce.vue')
              },
              {
                path: '/manageTestimony',
                name: 'ManageTestimony',
                component: () => import('./views/pages/Testimonies/AddTestimony.vue')
              },
              {
                path: '/viewTestimony',
                name: 'viewTestimony',
                component: () => import('./views/pages/Testimonies/ViewTestimony.vue')
              },
              {
                path: '/editTestimony',
                name: 'EditTestimony',
                component: () => import('./views/pages/Testimonies/EditTestimony.vue')
              },
              {
                path: '/authorizedTestimony',
                name: 'AuthorizedTestimonies',
                component: () => import('./views/pages/Testimonies/AuthorizedTestimonies.vue')
              },
              {
                path:'/addTithe',
                name:'AddTithe',
                component: () => import('./views/pages/Tithe/AddTithe.vue')
              },
              {
                path:'/editTithe',
                name:'EditTithe',
                component: () => import('./views/pages/Tithe/EditTithe.vue')
              },
              {
                path:'/viewTithe',
                name:'ViewTithe',
                component: () => import('./views/pages/Tithe/ViewTithe.vue')
              },
              // {
              //   path:'/addExpense',
              //   name:'AddExpense',
              //   component: () => import('./views/pages/Expenses/AddExpense.vue')
              // },
              // {
              //   path:'/editExpense',
              //   name:'EditExpense',
              //   component: () => import('./views/pages/Expenses/EditExpense.vue')
              // },
              // {
              //   path:'/viewExpense',
              //   name:'ViewExpense',
              //   component: () => import('./views/pages/Expenses/ViewExpense.vue')
              // },
              {
                path:'/addTotalAttendance',
                name:'addTotalAttendance',
                component: () => import('./views/pages/attendance/AddTotalAttendance.vue')
              },
              {
                path:'/editTotalAttendance',
                name:'addTotalAttendance',
                component: () => import('./views/pages/attendance/EditTotalAttendance.vue')
              },
              {
                path:'/viewAttendance',
                name:'ViewAttendance',
                component: () => import('./views/pages/attendance/viewAttendance.vue')
              },
            ],
        },
    // =============================================================================
    // FULL PAGE LAYOUTS
    // =============================================================================
        {
            path: '',
            component: () => import('@/layouts/full-page/FullPage.vue'),
            children: [
        // =============================================================================
        // PAGES
        // =============================================================================
            //     {
            //       path: '/',
            //       name: 'page-index',
            //       component: () => import('@/views/pages/index.vue')
            //   },
              {
                path: '/admin/ch/login',
                name: 'page-index',
                component: () => import('@/views/pages/Superadmin/Login.vue')
              },

              {
                path: '/login',
                name: 'page-login',
                component: () => import('@/views/pages/Login.vue')
              },
              {
                path: '/resetPassword',
                name: 'Church',
                component: () => import('@/views/pages/ResetPassword/ResetPassword.vue')
              },
              {
                path: '/pages/error-404',
                name: 'page-error-404',
                component: () => import('@/views/pages/Error404.vue')
              },
            ]
        },
        {
            // SUPER ADMIN PAGE
            path:'/admin/ch/dashboard',
            component: () => import('@/views/pages/Superadmin/Main.vue'),
            beforeEnter: (to, from, next) =>{
                console.log("before Enter ", isAuth);
                if(isAuth && auth !== undefined){
                  next()
                }
               else{ next("/admin/ch/login") }
            },
             children: [
                {
                    path: '/admin/ch/dashboard',
                    name: 'page-index',
                    component: () => import('@/views/pages/Superadmin/Dashboard.vue')
                },
                {
                    path: '/admin/ch/manageChurch',
                    name: 'ManageChurch',
                    component: () => import('@/views/pages/Superadmin/Churches/AddChurch.vue')
                },
                {
                    path: '/admin/ch/editChurch',
                    name: 'EditChurch',
                    component: () => import('@/views/pages/Superadmin/Churches/EditChurch.vue')
                },
                {
                    path: '/admin/ch/viewChurch',
                    name: 'ViewChurch',
                    component: () => import('@/views/pages/Superadmin/Churches/ViewChurch.vue')
                },
                {
                    path: '/admin/ch/churchAdmin',
                    name: 'AddChurchAdmins',
                    component: () => import('@/views/pages/Superadmin/Admins/AddChurchAdmin.vue')
                },
                {
                    path: '/admin/ch/viewChurchAdmins',
                    name: 'ViewChurch',
                    component: () => import('@/views/pages/Superadmin/Admins/ViewChurchAdmins.vue')
                },
                {
                    path: '/admin/ch/editChurchAdmin',
                    name: 'EditChurchAdmin',
                    component: () => import('@/views/pages/Superadmin/Admins/EditChurchAdmin.vue')
                },
                //
             ]
        },
        // Redirect to 404 page, if no match found
        {
            path: '*',
            redirect: '/pages/error-404'
        }
    ],
})

router.afterEach(() => {
  // Remove initial loading
  const appLoading = document.getElementById('loading-bg')
    if (appLoading) {
        appLoading.style.display = "none";
    }
})

// router.beforeEach((to, from, next) => {
//     if (to.name !=='/login' && !isAuth) next('/login')
//     else next()
//   })

export default router
