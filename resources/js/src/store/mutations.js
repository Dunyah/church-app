/*=========================================================================================
  File Name: mutations.js
  Description: Vuex Store - mutations
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import state from "./state"


const mutations = {


  // /////////////////////////////////////////////
  // COMPONENTS
  // /////////////////////////////////////////////

  // Vertical NavMenu

  TOGGLE_IS_VERTICAL_NAV_MENU_ACTIVE(state, value) {
    state.isVerticalNavMenuActive = value
  },
  TOGGLE_REDUCE_BUTTON(state, val) {
    state.reduceButton = val
  },
  UPDATE_MAIN_LAYOUT_TYPE(state, val) {
    state.mainLayoutType = val
  },
  UPDATE_VERTICAL_NAV_MENU_ITEMS_MIN(state, val) {
    state.verticalNavMenuItemsMin = val
  },
  UPDATE_VERTICAL_NAV_MENU_WIDTH(state, width) {
    state.verticalNavMenuWidth = width
  },
  UPDATE_MEMBER_DATA(state, payload){

    state.selectedMember.id = payload.id;
    state.selectedMember.name = payload.name;
    state.selectedMember.phoneNumber = payload.phoneNumber;
    state.selectedMember.photo = payload.photo;
    state.selectedMember.location = payload.location;
    state.selectedMember.occupation = payload.occupation;
    state.selectedMember.placeOfWork = payload.placeOfWork;
    state.selectedMember.MaritalStatus = payload.MaritalStatus;
    state.selectedMember.Gender = payload.Gender;
    state.selectedMember.contactPerson = payload.contactPerson;
    state.selectedMember.contactPersonPhoneNumber = payload.contactPersonPhoneNumber;
  },

  UPDATE_VISITOR_DATA(state, payload){
    state.selectedVisitor.id = payload.id;
    state.selectedVisitor.name = payload.name;
    state.selectedVisitor.phoneNumber = payload.phoneNumber;
    state.selectedVisitor.houseNo = payload.houseNo;
    state.selectedVisitor.emailAddress = payload.emailAddress;
    state.selectedVisitor.postalAddress = payload.postalAddress;
    state.selectedVisitor.invitedBy = payload.invitedBy;
    state.selectedVisitor.specificProblem = payload.specificProblem;
    state.selectedVisitor.prayerRequest = payload.prayerRequest;
    state.selectedVisitor.worshipwithusagain = payload.worshipwithusagain;
    state.selectedVisitor.ifNoWhy = payload.ifNoWhy;
    state.selectedVisitor.remarks = payload.remarks;
},

UPDATE_SERMON_DATA(state, payload){
 state.SelectedSermon.id = payload.id;
 state.SelectedSermon.title = payload.title;
 state.SelectedSermon.date = payload.date;
 state.SelectedSermon.description = payload.description;
 state.SelectedSermon.audio = payload.audio;
},
UPDATE_ANNOUNCEMENT_DATA(state, payload){
  state.SelectedAnnouncement.id = payload.id;
  state.SelectedAnnouncement.title = payload.title;
  state.SelectedAnnouncement.description = payload.description;
  state.SelectedAnnouncement.photo = payload.photo;
},
UPDATE_TESTIMONY_DATA(state, payload){
  state.SelectedTestimony.id = payload.id;
  state.SelectedTestimony.description = payload.description;
  state.SelectedTestimony.name = payload.name;
  state.SelectedTestimony.memberId = payload.memberId;
},
GET_SELECTED_EXCEL_FILE(state, payload){
   state.SelectExcelFile.path = payload.path;
},
UPDATE_TITHE_DATA(state,payload){
  state.SelectedTithe.id = payload.id;
  state.SelectedTithe.memberId = payload.memberId;
  state.SelectedTithe.name = payload.name;
  state.SelectedTithe.amount =payload.amount;
  state.SelectedTithe.datepaid = payload.datepaid;
},
GET_ALL_TITHES(state, payload){
   state.ArrayOfTithe.tithes = payload.tithes
},
GET_ALL_TESTIMONIES(state, payload){
    state.ArrayOfTestimonies.testimonies = payload.testimonies;
},
UPDATE_CHURCH_DATA(state, payload){
   state.SelectedChurch.id = payload.id;
   state.SelectedChurch.churchName = payload.churchName;
   state.SelectedChurch.contactNumber = payload.contactNumber;
   state.SelectedChurch.url = payload.url;
},
GET_ALL_CHURCHES(state, payload){
   state.GetAllChurches.churches = payload.churches;
},
GET_ALL_CHURCH_ADMINS(state,payload){
   state.GetChurchAdmin.churchAdmins = payload.churchAdmins;
},
UPDATE_CHURCH_ADMIN_DATA(state, payload){
   state.SelectChurchAdmin.id = payload.id;
   state.SelectChurchAdmin.name = payload.name;
   state.SelectChurchAdmin.email = payload.email;
   state.SelectChurchAdmin.churchName = payload.churchName;
   state.SelectChurchAdmin.churchId = payload.churchId;

},
GET_PASSWORD_STATUS_VALUE(state, payload){
state.GetPasswordStatus.password_status = payload.password_status;
state.GetPasswordStatus.jwt = payload.jwt;
},
GET_ALL_TOTAL_MEMBERS_FOR_A_DAY(state,payload){
 state.totalMembersPresentInService.totalMembersPresent = payload.totalMembersPresent;
},
UPDATE_ATTENDANCE_PER_SERVICE_DATA(state, payload){
   state.SelectedMembersPresentInService.id = payload.id;
   state.SelectedMembersPresentInService.datePresent = payload.datePresent;
   state.SelectedMembersPresentInService.numberOfPeople = payload.numberOfPeople;
},
  // VxAutoSuggest

  UPDATE_STARRED_PAGE(state, payload) {

    // find item index in search list state
    const index = state.navbarSearchAndPinList["pages"].data.findIndex((item) => item.url == payload.url)

    // update the main list
    state.navbarSearchAndPinList["pages"].data[index].is_bookmarked = payload.val

    // if val is true add it to starred else remove
    if (payload.val) {
      state.starredPages.push(state.navbarSearchAndPinList["pages"].data[index])
    }
    else {
      // find item index from starred pages
      const index = state.starredPages.findIndex((item) => item.url == payload.url)

      // remove item using index
      state.starredPages.splice(index, 1)
    }
  },

  // Navbar-Vertical

  ARRANGE_STARRED_PAGES_LIMITED(state, list) {
    const starredPagesMore = state.starredPages.slice(10)
    state.starredPages     = list.concat(starredPagesMore)
  },
  ARRANGE_STARRED_PAGES_MORE(state, list) {
    let downToUp                 = false
    let lastItemInStarredLimited = state.starredPages[10]
    const starredPagesLimited    = state.starredPages.slice(0, 10)
    state.starredPages           = starredPagesLimited.concat(list)

    state.starredPages.slice(0, 10).map((i) => {
      if (list.indexOf(i) > -1) downToUp = true
    })

    if (!downToUp) {
      state.starredPages.splice(10, 0, lastItemInStarredLimited)
    }
  },


  // ////////////////////////////////////////////
  // UI
  // ////////////////////////////////////////////

  TOGGLE_CONTENT_OVERLAY(state, val) { state.bodyOverlay       = val },
  UPDATE_PRIMARY_COLOR(state, val)   { state.themePrimaryColor = val },
  UPDATE_THEME(state, val)           { state.theme             = val },
  UPDATE_WINDOW_WIDTH(state, width)  { state.windowWidth       = width },
  UPDATE_WINDOW_SCROLL_Y(state, val) { state.scrollY = val },


  // /////////////////////////////////////////////
  // User/Account
  // /////////////////////////////////////////////

  // Updates user info in state and localstorage
  UPDATE_USER_INFO(state, payload) {

    // Get Data localStorage
    let userInfo = JSON.parse(localStorage.getItem("userInfo")) || state.AppActiveUser

    for (const property of Object.keys(payload)) {

      if (payload[property] != null) {
        // If some of user property is null - user default property defined in state.AppActiveUser
        state.AppActiveUser[property] = payload[property]

        // Update key in localStorage
        userInfo[property] = payload[property]
      }


    }
    // Store data in localStorage
    localStorage.setItem("userInfo", JSON.stringify(userInfo))
  },
}

export default mutations

