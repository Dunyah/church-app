export default {
  pages: {
    key: "title",
    data: [
      {title: 'Home',   url: '/dashboard',      icon: 'HomeIcon', is_bookmarked: false},
      {title: 'Visitor', url: '/addVisitor', icon: 'FileIcon', is_bookmarked: false},
      {title: 'Add Member', url: '/addMember', icon: 'userIcon', is_bookmarked: false},
      {title: 'View Member', url: '/viewMember', icon: 'userIcon', is_bookmarked: false},
      {title: 'Add Sermon', url: '/addSermon', icon: 'userIcon', is_bookmarked: false},
      {title: 'View Sermon', url: '/viewSermon', icon: 'userIcon', is_bookmarked: false},
      {title: 'Add Visitor', url: '/addVisitor', icon: 'userIcon', is_bookmarked: false},
      {title: 'View Visitor', url: '/viewVisitor', icon: 'userIcon', is_bookmarked: false},


    ]
  }
}
