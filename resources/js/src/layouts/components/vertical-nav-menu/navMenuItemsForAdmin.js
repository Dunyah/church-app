/*=========================================================================================
  File Name: sidebarItems.js
  Description: Sidebar Items list. Add / Remove menu items from here.
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


export default [
    {
      url: "/admin/ch/dashboard",
      name: "Home",
      slug: "home",
      icon: "HomeIcon",
    },
    {
        url: "/admin/ch/manageChurch",
        name: "Manage Church",
        slug: "church",
        icon: "CopyIcon",
    },
    {
        url: "/admin/ch/churchAdmin",
        name: "Setup Admin",
        slug: "addAdmin",
        icon: "UserIcon",
    },
    // {
    //       url: null, // You can omit this
    //       name: "Church Admins",
    //       slug: "admins",
    //       icon: "UserIcon",
    //       i18n: "Dashboard",
    //       submenu: [
    //           {
    //       url: "/admin/ch/churchAdmin",
    //       name: "Setup Admin",
    //       slug: "addAdmin",
    //       icon: "UserIcon",
    //     },
    //     {
    //       url: "/viewMember",
    //       name: "Manage Members",
    //       slug: "viewMember",
    //       icon: "GroupIcon",
    //     },
    //   ]
    // },


  ]
