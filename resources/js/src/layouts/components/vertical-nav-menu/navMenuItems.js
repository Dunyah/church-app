/*=========================================================================================
  File Name: sidebarItems.js
  Description: Sidebar Items list. Add / Remove menu items from here.
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


export default [
  {
    url: "/",
    name: "Home",
    slug: "home",
    icon: "HomeIcon",
  },
  {
		url: null, // You can omit this
		name: "Members",
		slug: "members",
		icon: "UserIcon",
		i18n: "Dashboard",
		submenu: [
			{
        url: "/addMember",
        name: "Add Members",
        slug: "addMember",
        icon: "UserIcon",
      },
      {
        url: "/viewMember",
        name: "Manage Members",
        slug: "viewMember",
        icon: "GroupIcon",
      },
      // {
      //   url: "/uploadMember",
      //   name: "Upload Member",
      //   slug: "uploadMember",
      //   icon: "UploadIcon",
      // },
		]
  },
  {
		url: null, // You can omit this
		name: "Visitors",
		slug: "Visitor",
		icon: "UserCheckIcon",
		i18n: "Dashboard",
		submenu: [
			{
        url: "/addVisitor",
        name: "Add Visitor",
        slug: "addVisitor",
      },
      {
        url: "/viewVisitor",
        name: "View Visitor",
        slug: "viewVisitor",
      },
		]
  },
  {
		url: "/addTithe",
		name: "Manage Tithes",
		slug: "Tithes",
        icon: "CommandIcon",
		i18n: "Dashboard",
  },
  {
		url: null, // You can omit this
		name: "Sermons",
		slug: "sermons",
		icon: "BookIcon",
		i18n: "Dashboard",
		submenu: [
			{
        url: "/addSermon",
        name: "Add Sermon",
        slug: "addSermon",
      },
      {
        url: "/viewSermon",
        name: "View Sermon",
        slug: "viewSermon",
      },
		]
  },
  // {
	// 	url: null, // You can omit this
	// 	name: "Expenses",
	// 	slug: "expenses",
	// 	icon: "BriefcaseIcon",
	// 	i18n: "Dashboard",
	// 	submenu: [
	// 		{
  //       url: "/addExpense",
  //       name: "Add Expense",
  //       slug: "addExpense",
  //     },
  //     {
  //       url: "/viewExpense",
  //       name: "View Expense",
  //       slug: "viewExpense",
  //     },
	// 	]
  // },
  {
		url: null, // You can omit this
		name: "Announcement",
		slug: "announcement",
		icon: "Volume1Icon",
		i18n: "Dashboard",
		submenu: [
			{
        url: "/addAnnouncement",
        name: "Add Announcement",
        slug: "addAnnouncement",
      },
      {
        url: "/viewAnnouncement",
        name: "View Announcement",
        slug: "viewAnnouncement",
      },
		]
  },
  {
		url: null, // You can omit this
		name: "Testimonies",
		slug: "testimonies",
		icon: "CommandIcon",
		i18n: "Dashboard",
		submenu: [
			{
        url: "/manageTestimony",
        name: "Manage Testimony",
        slug: "manageTestimony",
      },
      {
        url: "/authorizedTestimony",
        name: "Authorized Testimonies",
        slug: "authorizedTestimony",
      },
		]
  },
  {
    url: null, // You can omit this
    name: "Attendance",
    slug: "attendance",
    icon: "UsersIcon",
    i18n: "Dashboard",
    submenu: [
        {
    url: "/addTotalAttendance",
    name: "Manage Attendance",
    slug: "addTotalAttendance",
  },
  {
    url: "/viewAttendance",
    name: "View Attendance",
    slug: "viewAttendance",
  },
    ]
},
]
