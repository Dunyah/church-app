<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTithesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tithes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('member_id')->references('id')->on('members')->onDelete('cascade');
            $table->bigInteger('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->date('datepaid');
            $table->decimal('amount', 8, 2);
            $table->bigInteger('church_id')->references('id')->on('churches')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tithes');
    }
}
