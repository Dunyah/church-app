<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('phoneNumber')->unique();
            $table->string('occupation')->nullable();
            $table->string('placeOfWork')->nullable();
            $table->string('location')->nullable();
            $table->string('contactPerson')->nullable();
            $table->string('MaritalStatus')->nullable();
            $table->string('Gender')->nullable();
            $table->string('contactPersonPhoneNumber')->nullable();
            $table->string('photo')->nullable();
            $table->bigInteger('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('church_id')->references('id')->on('churches')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
