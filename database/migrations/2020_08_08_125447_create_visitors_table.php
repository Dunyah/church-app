<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('phoneNumber')->unique();
            $table->string('houseNo')->nullable();
            $table->string('emailAddress')->nullable();
            $table->string('postalAddress')->nullable();
            $table->string('invitedBy')->nullable();
            $table->string('specificProblem')->nullable();
            $table->string('prayerRequest')->nullable();
            $table->string('worshipwithusagain')->nullable();
            $table->string('ifNoWhy')->nullable();
            $table->string('remarks')->nullable();
            $table->bigInteger('church_id')->references('id')->on('churches')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitors');
    }
}
