<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::create([
            'name'=>'admin',
            'password' => Hash::make("password"),
            'email' => 'admin@eazichurch.com',
            'password_status' => 'RESET'
        ]);
    }
}
